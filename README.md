Apple's GeoServices.framework communicates with Apple servers
using HTTP requests with binary data.
This repository will have reverse-engineered documentation
and protobuf message definitions
for the request/response formats.

For example, the “manifest” containing information
about Apple Maps tile sets and URLs for other requests
can be decoded with:
```
wget -O geo_manifest.pb "https://gspe35-ssl.ls.apple.com/geo_manifest/dynamic/config?os=ios&os_version=12.4.8"
protoc --decode=Resources geoservices.proto < geo_manifest.pb
```

The `sample-PDPlaceRequest.pb` file in this repo contains data
sent from client to server
in a POST request to `https://gsp-ssl.ls.apple.com/dispatcher.arpc`.
You can decode it with:
```
protoc --decode=PDPlaceRequest geoservices.proto < sample-PDPlaceRequest.pb
```

Contributions to the .proto file are welcome!
Reverse-engineering this format is tedious but relatively easy.
You can load GeoServices.framework in your favorite decompiler,
and look for relatively simple code patterns
to figure out the protobuf tags.
See [reversing.md](reversing.md) for more info.
