Reverse-engineering GeoServices to figure out the protobuf format
is tedious but relatively easy.
Load GeoServices.framework (Mac or iOS, Intel or ARM, shouldn't matter much)
in your favorite decompiler, and look for some `GEO[Xxx]ReadSpecified` function.

(NOTE: it seems the protobuf "writing" functions `GEOXxxWriteTo` or `[GeoXxx writeTo:]`
are *much* easier to read than the "reading" functions.
This guide needs to be updated.)

For example, let's look at `GEOPDAnalyticMetadataReadSpecified`.
At some point the code reads a protobuf tag, shifts it 3 bits right,
and checks it with a switch():
```c
    uVar24 = uVar24 >> 3;
    switch((int)uVar24) {
    case 1:
      if ((cVar6 == '\0') ||
         ((*(byte *)(local_c0 + 1 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) & 4) != 0)) {
        cVar7 = __stubs::_PBReaderSkipValueWithTag(lVar16,1,bVar17);
        lVar16 = local_c8;
        uVar15 = local_d0;
        if (cVar7 == '\0') goto LAB_00ac3aed;
        param_3 = local_d8;
        if (cVar6 != '\0') {
          iVar10 = *(int *)(local_c0 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) << 0x15;
LAB_00ac3578:
          lVar25 = lVar25 + (iVar10 >> 0x1f);
          lVar16 = local_c8;
          param_3 = local_d8;
          uVar15 = local_d0;
        }
      }
      else {
        uVar13 = __stubs::_PBReaderReadString(lVar16);
        uVar13 = __stubs::_objc_retainAutoreleasedReturnValue(uVar13);
        lVar16 = _OBJC_IVAR_$_GEOPDAnalyticMetadata._appIdentifier;
LAB_00ac30bb:
        uVar12 = *(undefined8 *)(local_c0 + lVar16);
        *(undefined8 *)(local_c0 + lVar16) = uVar13;
        (*(code *)__got::_objc_release)(uVar12);
        lVar25 = lVar25 + -1;
        lVar16 = local_c8;
        param_3 = local_d8;
        uVar15 = local_d0;
      }
      break;
    case 2:
      if ((cVar6 != '\0') &&
         ((*(byte *)(local_c0 + 1 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) & 8) == 0)) {
        uVar13 = __stubs::_PBReaderReadString(lVar16);
        uVar13 = __stubs::_objc_retainAutoreleasedReturnValue(uVar13);
        lVar16 = _OBJC_IVAR_$_GEOPDAnalyticMetadata._appMajorVersion;
        goto LAB_00ac30bb;
      }
      cVar7 = __stubs::_PBReaderSkipValueWithTag(lVar16,2,bVar17);
      lVar16 = local_c8;
      uVar15 = local_d0;
      if (cVar7 == '\0') goto LAB_00ac3aed;
      param_3 = local_d8;
      if (cVar6 != '\0') {
        iVar10 = *(int *)(local_c0 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) << 0x14;
        goto LAB_00ac3578;
      }
      break;
```

There's no need to analyze this in too much detail.
If the tag is 1, the code reads a string
and saves it in `self.appIdentifier`.
If the tag is 2, the string is saved in `appMajorVersion`.
Everything else is checking for errors,
checking in `flags` whether `appIdentifier` was already read, etc.
and we can ignore it.

Our .proto file can now have:
```proto
message PDAnalyticMetadata {
    string appIdentifier = 1;
    string appMajorVersion = 2;
}
```

Another pattern to look for is nested objects:
```c
    case 8:
      if ((local_e8 & 1) != 0) {
        cVar6 = __stubs::_PBReaderPlaceMark(lVar16,local_100);
        lVar14 = local_c0;
        uVar15 = local_d0;
        if ((cVar6 == '\0') ||
           (cVar6 = _GEOSessionIDReadAllFrom
                              (_OBJC_IVAR_$_GEOPDAnalyticMetadata._sessionId + local_c0,lVar16,
                               (byte)local_d0 & 1), cVar6 == '\0')) goto LAB_00ac3aed;
        __stubs::_PBReaderRecallMark(lVar16,local_100);
        *(uint *)(lVar14 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) =
             *(uint *)(lVar14 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) | 1;
        param_3 = local_d8;
        break;
      }
      uVar13 = 8;
      cVar6 = __stubs::_PBReaderSkipValueWithTag(lVar16,uVar13,bVar17);
      goto LAB_00ac32c6;
```

If the tag is 8, it calls `GEOSessionIDReadAllFrom`
to parse a sub-object and store it directly into `self.sessionId`.
We add it like this:
```proto
message SessionID {}

message PDAnalyticMetadata {
    //...
    SessionID sessionId = 8;
}
```

Later we can decompile `GEOSessionIDReadAllFrom`
to figure out the contents of the `SessionID` object,
but for now we can leave it empty and just continue with this object.

Finally, objects can be repeated:
```c
    case 0xd:
      if ((cVar7 != '\0') ||
         ((*(byte *)(local_c0 + 2 + _OBJC_IVAR_$_GEOPDAnalyticMetadata._flags) & 4) != 0)) {
        uVar13 = 0xd;
        goto LAB_00ac329c;
      }
      cVar6 = __stubs::_PBReaderPlaceMark(lVar16,local_100);
      uVar15 = local_d0;
      if (cVar6 == '\0') goto LAB_00ac3aed;
      bVar17 = (byte)local_d0;
      uVar13 = __stubs::_objc_alloc_init(PTR_PTR_01739798);
      cVar6 = _GEOGeoServiceTagReadAllFrom(uVar13,lVar16,bVar17 & 1);
      if (cVar6 == '\0') goto LAB_00ac3e25;
      __stubs::_PBReaderRecallMark(lVar16,local_100);
      -[GEOPDAnalyticMetadata__addNoFlagsServiceTag:](local_c0,uVar13);
      (*(code *)__got::_objc_release)(uVar13);
      param_3 = local_d8;
      uVar15 = local_d0;
      break;
```
Here, if the tag is 0xd,
the code creates a new `GEOGeoServiceTag` object,
calls `GEOGeoServiceTagReadAllFrom` to parse it,
and calls `addNoFlagsServiceTag`,
which appends the object to a `self.serviceTags` array.

Once again we declare an empty `GeoServiceTag` message to be filled in later,
and we declare the field as `repeated`:

```proto
message GeoServiceTag {}
message PDAnalyticMetadata {
    //...
    repeated GeoServiceTag serviceTag = 0xd;
}
```
It can be useful to look at the [classdump for GeoServices](https://github.com/nst/iOS-Runtime-Headers/blob/master/PrivateFrameworks/GeoServices.framework/GEOPDAnalyticMetadata.h)
to have a better idea of the properties and types in these objects.
